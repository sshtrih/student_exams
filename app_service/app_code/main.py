import psycopg2
import os
import datetime

from prettytable import PrettyTable

connection = psycopg2.connect(user=os.environ['POSTGRES_USER'],
                              password=os.environ['POSTGRES_PASSWORD'],
                              host="postgres",
                              port=os.environ['POSTGRES_PORT'],
                              database=os.environ['POSTGRES_DB'])

cursor = connection.cursor()

cursor.execute('SELECT * FROM students WHERE EXTRACT(YEAR FROM AGE(NOW(), birthday)) < 22;')
result = cursor.fetchall()


table = PrettyTable()


table.field_names = ["ID", "Имя", "Фамилия", "Дата рождения"]

for row in result:
    table.add_row(row)

print(table)